package com.zenika.academy.jpabank;

import com.zenika.academy.jpabank.domain.BankAccount;
import com.zenika.academy.jpabank.domain.Customer;
import com.zenika.academy.jpabank.service.CustomersService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import java.util.Optional;

@SpringBootApplication
public class JpaBankApplication {

    public static void main(String[] args) {
        SpringApplication.run(JpaBankApplication.class, args);
    }

}

@Component
class Runner implements CommandLineRunner {

    private CustomersService customersService;

    Runner(CustomersService customersService) {
        this.customersService = customersService;
    }

    @Override
    public void run(String... args) throws Exception {

        Customer createdCustomer = customersService.createCustomer("Benoit");
        customersService.openAccount(createdCustomer.getId());

        // ---------------

        Optional<Customer> customer = customersService.getOne(createdCustomer.getId());
        customer.ifPresent(c -> {
            System.out.println(c.getAccounts());
        });
    }
}
