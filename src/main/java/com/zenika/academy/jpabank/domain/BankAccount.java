package com.zenika.academy.jpabank.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "accounts")
public class BankAccount {

    @Id
    private String id;
    private long number;
    private double balance;
    private String customerId;

    public BankAccount(String id, long number, String customerId) {
        this.id = id;
        this.number = number;
        this.customerId = customerId;
        this.balance = 0.0;
    }

    BankAccount() {
    }

    public String getId() {
        return id;
    }

    public long getNumber() {
        return number;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "number=" + number +
                ", balance=" + balance +
                '}';
    }
}
