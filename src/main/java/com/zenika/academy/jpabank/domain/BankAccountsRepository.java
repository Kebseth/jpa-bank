package com.zenika.academy.jpabank.domain;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BankAccountsRepository extends CrudRepository<BankAccount, String> {
    List<BankAccount> findByCustomerId(String customerId);
}
