package com.zenika.academy.jpabank.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "customers")
public class Customer {

    @Id
    private String id;
    private String name;

    @Transient
    private List<BankAccount> accounts = new ArrayList<>();

    public Customer(String id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * @deprecated Only for JPA
     */
    @Deprecated
    Customer() {
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<BankAccount> getAccounts() {
        return new ArrayList<>(accounts);
    }

    public void setAccounts(List<BankAccount> accountsOfCustomer) {
        this.accounts = new ArrayList<>(accountsOfCustomer);
    }
}
