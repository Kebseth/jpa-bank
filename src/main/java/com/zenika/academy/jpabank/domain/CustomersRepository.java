package com.zenika.academy.jpabank.domain;

import org.springframework.data.repository.CrudRepository;

public interface CustomersRepository extends CrudRepository<Customer, String> {
}
