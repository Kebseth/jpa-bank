package com.zenika.academy.jpabank.service;

import com.zenika.academy.jpabank.domain.BankAccount;
import com.zenika.academy.jpabank.domain.BankAccountsRepository;
import com.zenika.academy.jpabank.domain.Customer;
import com.zenika.academy.jpabank.domain.CustomersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class CustomersService {

    private CustomersRepository customersRepository;
    private BankAccountsRepository bankAccountsRepository;

    @Autowired
    public CustomersService(CustomersRepository customersRepository, BankAccountsRepository bankAccountsRepository) {
        this.customersRepository = customersRepository;
        this.bankAccountsRepository = bankAccountsRepository;
    }

    public Optional<Customer> getOne(String id) {
        final Optional<Customer> customer = customersRepository.findById(id);
        customer.ifPresent(c -> {
            List<BankAccount> accountsOfCustomer = bankAccountsRepository.findByCustomerId(c.getId());
            c.setAccounts(accountsOfCustomer);
        });
        return customer;
    }

    public Customer createCustomer(String name) {
        Customer newCustomer = new Customer(UUID.randomUUID().toString(), name);
        return customersRepository.save(newCustomer);
    }

    public void updateCustomer(String id, String newName) {
        customersRepository.findById(id).ifPresent(c -> {
            Customer updatedCustomer = new Customer(id, newName);
            customersRepository.save(updatedCustomer);
        });
    }

    public BankAccount openAccount(String customerId) {
        BankAccount openedAccount = new BankAccount(UUID.randomUUID().toString(), 1234, customerId);
        return bankAccountsRepository.save(openedAccount);
    }
}
