create table accounts (
                          id text primary key ,
                          number bigint not null,
                          balance double precision
);

alter table customers drop constraint customers_name_key;